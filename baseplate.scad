include <config/config.scad>

use <y_end.scad>

module baseplate()
{
    difference()
    {
        square([footprint_x, footprint_y], center=true);
        // y_ends
        for(i=[-1,1])
        for(j=[-1,1])
        translate([(y_axis_seperation/2)*i,0,0])
        translate([0, (y_axis_length/2)*j - (y_end_depth/2)*j,0])
        translate([0,0,y_axis_z_position])
        rotate([0,0,90*(j-1)])
        {
            y_end_mount_holes();
        }
    }
}

baseplate();
