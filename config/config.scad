// 3D print settings
min_wall = 2;
layer_height = 0.25;

// smooth rod pressure gap
smooth_rod_gap = layer_height*2;

// Thickness of the baseplate material
baseplate_thickness = 12;

// Smooth rod
smooth_rod_d = 8;

// Bearing
bearing_od = 15;
bearing_id = 8;
bearing_length = 24;

// Motor size
nema_size = 42;
nema_length = 38;

// PCB work area
work_area_x = 100;
work_area_y = 100;
work_area_z = 30;

// Carriage size
x_carriage_width = nema_size + (2*bearing_od) + (4*min_wall);
y_carriage_width = nema_size + (2*bearing_od) + (4*min_wall);
y_carriage_length = nema_size + bearing_od/2 + min_wall;

// y end size
y_end_depth = 15;

// spindle offset
spindle_offset = 50;

// Machine footprint
footprint_x = work_area_x + x_carriage_width + (2*y_carriage_length) + 10;
footprint_y = work_area_y + y_carriage_length + spindle_offset + (2*y_end_depth) + 10;

// X-axis
x_axis_seperation = nema_size + bearing_od + (2*min_wall);
x_axis_length = footprint_x;

// Y-axis
y_axis_seperation = footprint_x - (2*40);
y_axis_length = footprint_y;
y_axis_z_position = work_area_z - 15;

echo("Footprint Y:", footprint_y);
echo("Footprint X:", footprint_x);
