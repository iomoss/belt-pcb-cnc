include <config/config.scad>

// animation position
x_pos = 0;
y_pos = 60;
z_pos = 0;

// work area
%translate([0,-footprint_y/2+work_area_y/2+y_end_depth,0]) square([work_area_x, work_area_y], center=true);

// base plate
use <baseplate.scad>
color("Khaki")
translate([0,0,-baseplate_thickness])
linear_extrude(height=baseplate_thickness)
baseplate();

// x-axis
color("silver")
for(i=[-1,1])
translate([0,(x_axis_seperation/2)*i+y_pos,work_area_z+bearing_od/2 + 1])
rotate([0,90,0])
    cylinder(r=smooth_rod_d/2, h=x_axis_length, center=true);

// y-axis
color("silver")
for(i=[-1,1])
translate([(y_axis_seperation/2)*i,0,y_axis_z_position])
rotate([90,0,0])
    cylinder(r=smooth_rod_d/2, h=y_axis_length, center=true);

// xy carriage
use <xy_carrier.scad>
for(i=[0,1]) translate([i*(y_axis_seperation)-y_axis_seperation/2,y_pos,y_axis_z_position]) mirror([i,0,0]) xy_carriage();

// y-axis ends
use <y_end.scad>
for(i=[-1,1])
for(j=[-1,1])
translate([(y_axis_seperation/2)*i,0,0])
translate([0, (y_axis_length/2)*j - (y_end_depth/2)*j,0])
translate([0,0,y_axis_z_position])
rotate([0,0,90*(j-1)])
{
    y_end();
    y_end_top();
}

// belts
%for(i=[-1,1]) translate([i*footprint_x/2,0,work_area_z]) cube([1.5,footprint_y,5], center = true);
