include <config/config.scad>
use <Library/nema_17.scad>
use <Library/LM8UU.scad>

z_pos=-work_area_z+0;
z_clearance=5;
z_lead_screw_d=6;

%for(i=[-1,1]) translate([0,i*x_axis_seperation/2,z_clearance+bearing_od/2]) rotate([0,90,0]) cylinder(r=8/2, h=x_axis_length, center=true);
%for(i=[-1,1], j=[-1,1]) translate([j*20,i*x_axis_seperation/2,z_clearance+bearing_od/2]) rotate([0,90,0]) LM8UU();
translate([0,-x_axis_seperation/2-25,25+z_pos]) spindle();
%translate([0,x_axis_seperation/2-5.5-4,42/2+15+z_clearance+bearing_od/2]) rotate([-90,0,0]) nema_17();
z_axis();

module z_axis() {

    %for(i=[-1,1]) translate([i*25,-x_axis_seperation/2-smooth_rod_d/2-1-bearing_od/2,10]) cylinder(r=8/2, h=135);
    %translate([0,-x_axis_seperation/2-smooth_rod_d/2-1-bearing_od/2,z_pos+145]) rotate([180,0,0]) cylinder(r=z_lead_screw_d/2, h=work_area_z+30);
    %for(i=[0,1], j=[-1,1]) translate([j*25,-x_axis_seperation/2-smooth_rod_d/2-1-bearing_od/2,i*56+bearing_length/2+53+z_pos]) rotate([0,0,0]) LM8UU();
    %translate([0,-x_axis_seperation/2-smooth_rod_d/2-1-z_lead_screw_d/2,z_pos+165]) rotate([180,0,0]) nema_17();

}

module spindle() {
    %translate([0,0,116]) rotate(180) gt2_172_belt();
    %translate([0,50,126+7]) rotate([0,180,0]) gt2_48_pulley();
    %translate([0,50,127-50]) ST3511_brushless_motor();
    %translate([0,0,108]) gt2_20_pulley(r=8);
    %for(i=[0,1]) translate([0,0,i*73+28]) 608_bearing(center=false);
    %collet();
    difference() {
        union() {

        }
    }
}

module collet() {
    color("Silver") cylinder(r=8/2, h=127);
    color("Silver") cylinder(r=14/2, h=27);
    color("Grey") translate([0,0,7]) cylinder(r=19/2, h=6);
    color("Grey") cylinder(r=19/2, h=7, $fn=6);
}

module 608_bearing(center=true) {
    color("Silver") difference() {
        cylinder(r=22/2, h=7, center=center);
        translate([0,0,-0.1])cylinder(r=8/2, h=7.3, center=center);
    }
}

module gt2_20_pulley(r=5) {
    color("Silver") difference() {
        union() {
            translate([0,0,15]) cylinder(r=16/2, h=1);
            cylinder(r=12/2, h=16, $fn=12);
            cylinder(r=16/2, h=6.5);
        }
        translate([0,0,-0.1]) cylinder(r=r/2, h=16.2);
    }
}

module gt2_48_pulley(r=5) {
    color("Silver") difference() {
        union() {
            translate([0,0,19.5]) cylinder(r=36/2, h=1);
            translate([0,0,8]) cylinder(r=30/2, h=12.5, $fn=12);
            translate([0,0,8]) cylinder(r=36/2, h=1);
            cylinder(r=24/2, h=8);
        }
        translate([0,0,-0.1]) cylinder(r=r/2, h=20.7);
    }
}

module gt2_172_belt() {
    color("Black") difference() {
        hull() {
            translate([0,0,0]) cylinder(r=14/2, h=6);
            translate([0,-50,0]) cylinder(r=31.5/2, h=6);
        }
        hull() {
            translate([0,0,-0.1]) cylinder(r=12/2, h=6.2);
            translate([0,-50,-0.1]) cylinder(r=29.5/2, h=6.2);
        }
    }
}

module ST3511_brushless_motor() {
    color("Black") translate([0,0,30]) cylinder(r=15/2, h=4.5);
    color("Black") translate([0,0,11]) cylinder(r=42/2, h=19);
    color("Silver") translate([0,0,2]) cylinder(r=5/2, h=50);
    rotate(45) difference() {
        color("Silver") for(i=[0,1]) translate([0,0,2.7/2]) rotate(i*90) cube([9.5,62,2.7], center=true);
        for(i=[-1,1]) translate([52/2*i,0,-0.1]) cylinder(r=4/2, h=3);
        for(i=[-1,1]) translate([0,52/2*i,-0.1]) cylinder(r=4/2, h=3);
    }
}
