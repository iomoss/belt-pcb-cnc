include <config/config.scad>
include <config/nut_sizes.scad>

use <Library/cylinder_poly.scad>

wall_thickness = 4;

// Y-axis
%rotate([90,0,0])
cylinder(r=smooth_rod_d/2, h=50, center=true);

module smooth_rod_capture(top = true)
{
    if(top)
        smooth_rod_capture_worker(-1);
    else
        smooth_rod_capture_worker(1);
}

module smooth_rod_capture_worker(top)
{
    difference()
    {
        hull()
        {
            rotate([90,0,0])
                cylinder(r=smooth_rod_d/2 + wall_thickness, h=y_end_depth, center=true);

            for(i=[-1,1])
                translate([(smooth_rod_d/2+wall_thickness) * i,0,0])
                    cylinder(r=y_end_depth/2, h=smooth_rod_d+wall_thickness*2, center=true);
        }
        rotate([90,0,0])
            cylinder(r=smooth_rod_d/2, h=y_end_depth+1, center=true);

        for(i=[-1,1])
            translate([(smooth_rod_d/2+wall_thickness) * i,0,0])
                cylinder_poly(r=3/2, h=smooth_rod_d+wall_thickness*2+2, center=true);
        translate([0,0,(10 - smooth_rod_gap/2) * top])
        cube([smooth_rod_d+wall_thickness*2+y_end_depth+1, y_end_depth+1,20], center=true);
    }
}

module y_end_top()
{
    smooth_rod_capture(true);
}

module y_end_mount_holes()
{
    projection(){
        for(i=[-1,1])
            translate([(smooth_rod_d*2+wall_thickness) * i,0,-y_axis_z_position+M4_nut_h])
        {
            rotate([0,0,30])
            cylinder_poly(r=4/2, h=20, center=true);
        }
    }
}

module y_end_bot()
{
    difference()
    {
        smooth_rod_capture(false);

        translate([0,0,-(smooth_rod_d/2+wall_thickness) + M3_nut_h/2])
        for(i=[-1,1])
        hull()
        {
            translate([(smooth_rod_d/2+wall_thickness) * i,0,0])
            for(j=[0,1])
            {
                translate([0,10*j,0])
                rotate([0,0,30])
                cylinder(r=M3_nut/2, h=M3_nut_h, center=true, $fn=6);
            }
        }
    }
}

module y_end()
{
    y_end_bot();

    difference()
    {
        hull()
        {
        for(i=[-1,1])
            translate([(smooth_rod_d*2+wall_thickness) * i,0,-(smooth_rod_d/2+wall_thickness)-1/2])
                cylinder(r=y_end_depth/2, h=1, center=true);

        for(i=[-1,1])
            translate([(smooth_rod_d*2+wall_thickness) * i,0,-y_axis_z_position])
                cylinder(r=y_end_depth/2, h=1, center=true);
        }

        for(i=[-1,1])
            translate([(smooth_rod_d*2+wall_thickness) * i,0,-y_axis_z_position+M4_nut_h])
        {
            rotate([0,0,30])
            cylinder(r=M4_nut/2, h=M4_nut_h, center=true, $fn=6);
            cylinder_poly(r=4/2, h=20, center=true);
        }
        
        for(i=[-1,1])
            translate([(smooth_rod_d/2+wall_thickness) * i,0,-y_axis_z_position/2])
                cylinder_poly(r=3/2, h=y_axis_z_position+2, center=true);

        
    }
}

y_end();
y_end_top();
