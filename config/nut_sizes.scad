//-----------
// Nut sizes
//-----------
// INFO: Adjustable for imprecise printers
// INFO: All os there are diameters
// INFO: Unit is millimeters
//----
// M3
//----
M3 = 3.2;
M3_nut = 6.6;
M3_nut_h = 2.5;
M3_top_d = 7;

//----
// M4
//----
M4 = 4.2;
M4_nut = 8.2;
M4_nut_h = 5;

//----
// M5
//----
M5 = 5.1;
M5_top_d = 10;
M5_nut = 9.4;
M5_nut_low_h = 2.4;
M5_lock_nut_h = 6.5;
M5_nut_h = 4.3;

//----
// M6
//----
M6 = 6.2;
M6_nut = 11.6;


