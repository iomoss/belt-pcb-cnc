include <config/config.scad>
include <config/nut_sizes.scad>

use <Library/nema_17.scad>
use <Library/LM8UU.scad>
use <Library/cylinder_poly.scad>

xy_carriage();

plastic_thickness = 5;
belt_bearing_thickness = 8;
m3_washer_thickness = 0.5;
motor_length = 50;
// TODO: REPLACE +1 with z_clearance
z_clearance = 5;

module belt_bearing() {
	translate([0,0,-2])
	cylinder(r=10/2, h=4, center=true);
	translate([0,0,2])
	cylinder(r=10/2, h=4, center=true);
}

// x-axis
%for(i=[-1,1])
translate([y_axis_seperation/2,(x_axis_seperation/2)*i,work_area_z+bearing_od/2 + 1])
rotate([0,90,0])
    cylinder(r=smooth_rod_d/2, h=x_axis_length, center=true);

module xy_carriage() {
   %translate([-(plastic_thickness + belt_bearing_thickness/2 + m3_washer_thickness),0,(42+5)/2+bearing_od/2 + 1+5/2]) rotate([0,90,0]) nema_17();
   %for(i=[-1,1]) translate([0,i*bearing_length,0]) rotate([90,0,0]) LM8UU();
   %for(i=[-1,1]) translate([0, 31/2 * i, 0])
translate([0,0,(42+5)/2 + bearing_od/2 + 1 - 15.5 + 5/2])
   rotate([0,90,0])
   belt_bearing();
}

translate([-(belt_bearing_thickness/2 + m3_washer_thickness),0,0])
translate([0,0,(42+5)/2 + bearing_od/2 + 1])
translate([-motor_length/2, 0, 0])
difference()
{
	// TODO: Fix Nema 17 position
    cube([motor_length, 42+20*2, 42+5], center=true);
    translate([-5,0,5/2])
    cube([motor_length+1, 42+0.1, 42+0.1], center=true);

translate([motor_length/2,0,5/2])
	rotate([0,90,0])
	translate([0,0,-15])
	nema_holes(ring=true, holes=true);

for(i=[-1,1])
translate([0,0,-((42+5)/2 + bearing_od/2 + 1)])
translate([0,0,work_area_z + bearing_od/2 + 1])
translate([0, -i*(x_axis_seperation/2),0])
rotate([90*i,0,0])
rotate([0,0,90])
	bearing(motor_length, 1);
}

wall_thickness = 4;
// TODO: Nut traps inside

module bearing(length = 50, top=1)
{
	rotate([90,0,0])
		cylinder(r=smooth_rod_d/2, h=length+1, center=true);

	assign(step_size = 4)
		for(j=[0 : step_size : length])
			for(i=[-1,1])
			{
				translate([(smooth_rod_d/2+wall_thickness) * i, -length/2 + 10 + j*step_size, -10/2 * top])
				{
					cylinder_poly(r=M3/2, h=10, center=true);
				if(top != -1)
				translate([0,0,(-10/2+M3_nut_h/2+0.5) * top])
				cylinder(r=M3_nut/2, h=M3_nut_h, $fn=6, center=true);
}
			}
	translate([0,0,(10 - smooth_rod_gap/2) * top])
		cube([smooth_rod_d+wall_thickness*2+42+1+5, length+1,20], center=true);
}

for(i=[-1,1])
translate([-(belt_bearing_thickness/2 + m3_washer_thickness),0,0])
translate([0,0,work_area_z + bearing_od/2 + 1])
translate([-motor_length/2, 0, 0])
translate([0, -i*(x_axis_seperation/2),0])
rotate([90*i,0,0])
rotate([0,0,90])
difference()
{
translate([0,0,5/2])
cube([68/2, motor_length, 12.5], center=true);
bearing(length=motor_length, top=-1);
}

