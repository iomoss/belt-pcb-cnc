include<../config/config.scad>

$fn=25;

LM8UU_metal_d = 2;
LM8UU_rubber_d = bearing_od - LM8UU_metal_d;

module LM8UU()
{
    difference()
    {
        color("gray")
        cylinder(r=bearing_od/2, h=bearing_length, center=true);
        cylinder(r=LM8UU_rubber_d/2, h=bearing_length+0.1, center=true);
        // Ribs
        for( i = [-1,1] )
        translate([0,0,(bearing_length/2 -1 -3) * i])
        difference()
        {
            cylinder(r=(bearing_od+0.1)/2, h=1, center=true);
            cylinder(r=(bearing_od - 1)/2, h=1, center=true);
        }
    }
    difference()
    {
        color("black")
        cylinder(r=LM8UU_rubber_d/2, h=23, center=true);
        cylinder(r=bearing_id/2, h=24, center=true);
    }
}

LM8UU();
